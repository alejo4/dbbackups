FROM golang:1.22.3-alpine3.20 as build
WORKDIR /app
COPY go.mod go.sum /app/
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/database-backups cmd/export/main.go && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/clear-backups    cmd/clear/main.go

FROM alpine:3.20
RUN apk --no-cache add ca-certificates mysql-client zlib
WORKDIR /app
COPY --from=build /app/build /app
COPY --from=build /usr/local/go/lib/time/zoneinfo.zip /
ENV ZONEINFO=/zoneinfo.zip
