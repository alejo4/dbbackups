package backups

import (
	"context"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// Upload a backup to a s3 bucket.
func Upload(ctx context.Context, reader io.ReadSeeker, filename string) error {
	_, err := s3Client().PutObject(&s3.PutObjectInput{
		ACL:          aws.String("private"),
		Body:         reader,
		Bucket:       aws.String(bucket()),
		Key:          aws.String(path.Join(prefix(), filename)),
		StorageClass: aws.String(s3.StorageClassGlacier),
	})

	if err != nil {
		return fmt.Errorf("backups.Upload: failed to upload the backup file: %w", err)
	}

	return nil
}

// Clear removes the old backup files in the bucket.
func Clear(ctx context.Context, threshold time.Duration) error {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket()),
		Prefix: aws.String(prefix()),
	}

	objects, err := s3Client().ListObjects(input)
	if err != nil {
		fmt.Println(err.Error())
	}

	for _, obj := range objects.Contents {
		fileNameParts := strings.Split(strings.ReplaceAll(*obj.Key, ".zip", ""), "/")
		fileDate := fileNameParts[len(fileNameParts)-1]

		fileTime, err := time.Parse("2006-01-02-15-04", fileDate)
		if err != nil {
			return fmt.Errorf("backups.Clear: failed to parse file name, expect a valid date Y-m-d-h-i-s: %w", err)
		}

		if !time.Now().Add(-threshold).After(fileTime) {
			continue
		}

		if err := deleteFile(ctx, *obj.Key); err != nil {
			return err
		}
	}

	return nil
}

func deleteFile(ctx context.Context, filename string) error {
	_, err := s3Client().DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(bucket()),
		Key:    aws.String(filename),
	})

	if err != nil {
		return fmt.Errorf("backups.deleteFile: failed to remove the old backup file %q: %w", filename, err)
	}

	return nil
}

func s3Client() *s3.S3 {
	key := os.Getenv("AWS_KEY")
	secret := os.Getenv("AWS_SECRET")

	s3Config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(key, secret, ""),
		Endpoint:    aws.String(os.Getenv("AWS_S3_ENDPOINT")),
		Region:      aws.String(region()),
	}

	newSession := session.New(s3Config)

	return s3.New(newSession)
}

func bucket() string {
	return os.Getenv("AWS_BUCKET")
}

func region() string {
	return os.Getenv("AWS_REGION")
}

func prefix() string {
	v := os.Getenv("AWS_PREFIX")
	if v == "" {
		return "backups"
	}

	return v
}
