IMAGE_TAG := $(shell git describe --tags)
SERVICE_IMAGE = lapix/mysqldump
ARCH = linux/amd64,linux/arm64

publish:
	docker buildx build \
 		--builder container \
		--build-arg ENVIRONMENT=$(ENVIRONMENT) \
		--file Dockerfile \
		--platform $(ARCH) \
		--push \
		--tag $(SERVICE_IMAGE):$(IMAGE_TAG) \
		.
