package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/lapixdevelopment/nomadu/backend/backups"
)

func main() {
	flag.Parse()
	durationString := flag.Arg(0)
	if durationString == "" {
		flag.Usage()
		os.Exit(1)
	}

	duration, err := time.ParseDuration(durationString)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to parse the duration: %s", err.Error())
		os.Exit(2)
	}

	err = backups.Clear(context.Background(), duration)

	if err != nil {
		log.Fatal(err)
	}
}
