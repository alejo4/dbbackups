package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/lapixdevelopment/nomadu/backend/backups"

	"github.com/joho/godotenv"

	_ "github.com/go-sql-driver/mysql"
)

var conf = backups.DBConfig{}

var timeout string

func init() {
	flag.StringVar(&conf.Host, "host", "localhost", "host connection")
	flag.StringVar(&conf.Port, "port", "3306", "connection port")
	flag.StringVar(&conf.Username, "user", "", "username")
	flag.StringVar(&timeout, "timeout", "1m", "timeout")

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	conf.Password = os.Getenv("DB_PASSWORD")
}

// Usage is a replacement usage function for the flags.
func Usage() {
	fmt.Fprintf(os.Stderr, "Usage of MariaDB backup: \n")
	fmt.Fprintf(os.Stderr, "\tbackup [flags]\n")
	flag.PrintDefaults()
}

func main() {
	flag.Usage = Usage
	flag.Parse()

	timeoutDuration, err := time.ParseDuration(timeout)
	if err != nil {
		log.Fatal(err, conf)
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeoutDuration)
	defer cancel()

	log.Println("Creating the dump file...")
	file, err := backups.Build(ctx, conf)
	if err != nil {
		log.Fatal(err, conf)
	}

	log.Println("Uploading the backup to S3...")
	if err := backups.Upload(ctx, file, name()); err != nil {
		log.Fatal(err)
	}
}

func name() string {
	now := time.Now()

	return now.Format("2006-01-02-15-04") + ".zip"
}
