package backups

import (
	"archive/zip"
	"context"
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

// DBConfig hold the common database configuration.
type DBConfig struct {
	Host     string
	Port     string
	Username string
	Password string
}

var exclude = []string{
	"mysql",
	"test",
	"sys",
	"performance_schema",
	"information_schema",
}

// mysqldump created a backup from a given database, it will write the content
// to the given write.
func mysqldump(ctx context.Context, config DBConfig, database string, writer io.Writer) (int64, error) {
	cmd := exec.Command(
		command(),
		fmt.Sprintf("-P%s", config.Port),
		fmt.Sprintf("-h%s", config.Host),
		fmt.Sprintf("-u%s", config.Username),
		fmt.Sprintf("-p%s", config.Password),
		"--quick",
		"--compress",
		database,
	)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return 0, fmt.Errorf("mysqldump: failed to get the stdout from command: %w", err)
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return 0, fmt.Errorf("mysqldump: failed to get the stderr from command: %w", err)
	}

	if err := cmd.Start(); err != nil {
		return 0, fmt.Errorf("mysqldump: failed to execute the mysqldump command: %w", err)
	}

	n, err := io.Copy(writer, stdout)
	if err != nil {
		return 0, fmt.Errorf("mysqldump: fail while copying the backup data: %w", err)
	}

	output := make([]byte, 1024)
	stderr.Read(output)

	if err := cmd.Wait(); err != nil {
		return 0, fmt.Errorf("mysqldump: failed to create the dump for database '%s'.\n%s\n%w", database, string(output), err)
	}

	return n, nil
}

// Build builds a file with the dump of the available schemas in the database.
func Build(ctx context.Context, config DBConfig) (io.ReadSeeker, error) {
	file, err := ioutil.TempFile("", "*.dump")
	if err != nil {
		return nil, fmt.Errorf("Build: failed to create the temporary file. %w", err)
	}

	log.Printf("Writing to temporal file at '%s'", file.Name())

	if err := buildZIP(ctx, file, config); err != nil {
		return nil, fmt.Errorf("Build: failed to create the temporary file. %w", err)
	}

	if _, err := file.Seek(0, 0); err != nil {
		return nil, fmt.Errorf("Build: failed to seek the temporary file to the beginning. %w", err)
	}

	return file, nil
}

// Build builds a file with the dump of the available schemas in the database.
func buildZIP(ctx context.Context, w io.Writer, config DBConfig) error {
	databases, err := showDatabases(ctx, config)
	if err != nil {
		return err
	}

	zipFile := zip.NewWriter(w)
	for _, database := range databases {
		log.Printf("Exporting database %s", database)

		entryName := fmt.Sprintf("%s.sql", database)
		zipEntry, err := zipFile.Create(entryName)
		if err != nil {
			return fmt.Errorf("Build: failed to write to the zip entry %q: %w", entryName, err)
		}

		if _, err := mysqldump(ctx, config, database, zipEntry); err != nil {
			return err
		}
	}

	if err := zipFile.Close(); err != nil {
		return fmt.Errorf("Build: failed to close the zip file: %w", err)
	}

	return nil
}

func showDatabases(ctx context.Context, config DBConfig) ([]string, error) {
	db, err := sql.Open("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/",
		config.Username,
		config.Password,
		config.Host,
		config.Port,
	))

	if err != nil {
		return nil, fmt.Errorf("showDatabases: failed to connect to the DB server: %w", err)
	}

	rows, err := db.QueryContext(ctx, "SHOW DATABASES")
	if err != nil {
		return nil, fmt.Errorf("showDatabases: failed to list the databases: %w", err)
	}

	defer rows.Close()

	databases := make([]string, 0)

	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, fmt.Errorf("showDatabases: error while reading databases list: %w", err)
		}

		if !excluded(name) {
			databases = append(databases, name)
		}
	}

	return databases, err
}

func excluded(find string) bool {
	for _, v := range exclude {
		if v == find {
			return true
		}
	}

	return false
}

func command() string {
	path := os.Getenv("MYSQLDUMP_BIN")
	if path != "" {
		return path
	}

	return "mysqldump"
}
