module bitbucket.org/lapixdevelopment/nomadu/backend/backups

go 1.22

require (
	github.com/aws/aws-sdk-go v1.38.41
	github.com/go-sql-driver/mysql v1.6.0
	github.com/joho/godotenv v1.5.1
)

require github.com/jmespath/go-jmespath v0.4.0 // indirect
